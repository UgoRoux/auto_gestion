

Utilisation :

1 se placer dans le dossier auto_gestion

2 lancer ./auto_gestion.sh LE_MOT_CLE

OPTIONS :
        
        
    ./auto_gestion.sh -i LE_MOT_CLE    =  Ouvre le images liees a ce mot cle
    
    (utile pour ouvrir les images)
    
    
    ./auto_gestion.sh -w LE_MOT_CLE    =  ne cherche que les mots qui ont 
                                          exactement la meme orthographe 
                                          que LE_MOT_CLE

    (par mal si le truc qu'on cherche est assez court)
    
    
    
    [on peut associer les deux : -wi et -iw marchent]